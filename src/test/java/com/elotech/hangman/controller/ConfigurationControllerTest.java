package com.elotech.hangman.controller;

import com.elotech.hangman.dto.ConfigurationDTO;
import com.elotech.hangman.service.Impl.ConfigurationServiceImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.BDDMockito.*;

@RunWith(SpringRunner.class)
@WebMvcTest(ConfigurationController.class)
@ActiveProfiles("test")
public class ConfigurationControllerTest {

    @MockBean
    private ConfigurationServiceImpl configurationServiceImpl;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private Gson gson;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        gson = new GsonBuilder().serializeNulls().create();

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void shouldSaveConfiguration() throws Exception {
        final var gameConfiguration = ConfigurationDTO
                .builder()
                .attempts(3)
                .secretWord("Amigo")
                .build();
        given(configurationServiceImpl.save(refEq(gameConfiguration))).willReturn(gameConfiguration);

        this.mockMvc.perform(post("/v1/configuration")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(gson.toJson(gameConfiguration)))
                .andDo(print())
                .andExpect(status().isOk());

        verify(configurationServiceImpl).save(refEq(gameConfiguration));
    }


}
