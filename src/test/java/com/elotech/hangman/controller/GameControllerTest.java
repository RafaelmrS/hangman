package com.elotech.hangman.controller;

import com.elotech.hangman.service.Impl.GameServiceImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(GameController.class)
@ActiveProfiles("test")
public class GameControllerTest {

    @MockBean
    private GameServiceImpl gameServiceImpl;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private Gson gson;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        gson = new GsonBuilder().serializeNulls().create();

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void shouldExecuteGameWithTheLetters() throws Exception {
        final var response = "A____ letter entered successfully";

        given(gameServiceImpl.execute(1L, "a")).willReturn(response);

        this.mockMvc.perform(post("/v1/game/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(gson.toJson('A')))
                .andDo(print())
                .andExpect(status().isOk());

        verify(gameServiceImpl).execute(1L, "a");
    }

    @Test
    public void shouldGetAttempts() throws Exception {

        given(gameServiceImpl.getAttempts(1L)).willReturn(any());

        this.mockMvc.perform(get("/v1/game/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().isOk());

        verify(gameServiceImpl).getAttempts(1L);
    }
}
