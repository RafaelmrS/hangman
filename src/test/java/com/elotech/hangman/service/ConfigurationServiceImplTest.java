package com.elotech.hangman.service;

import com.elotech.hangman.dto.ConfigurationDTO;
import com.elotech.hangman.entity.Game;
import com.elotech.hangman.repository.ConfigurationJpaRepository;
import com.elotech.hangman.service.Impl.ConfigurationServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class ConfigurationServiceImplTest {

    @Mock
    private ConfigurationJpaRepository configurationJpaRepository;

    @InjectMocks
    private ConfigurationServiceImpl configurationServiceImpl;

    @Test
    public void shouldSaveConfiguration() {
        final var game = Game
                .builder()
                .attempts(3)
                .id(1L)
                .letter("A")
                .secretWord("Amigo")
                .build();
        final var gameConfiguration = ConfigurationDTO
                .builder()
                .attempts(3)
                .secretWord("Amigo")
                .build();

        when(configurationJpaRepository.save(any(Game.class))).thenReturn(game);

        final var result = configurationServiceImpl.save(gameConfiguration);

        verify(configurationJpaRepository).save(any(Game.class));

        assertThat(result.getSecretWord()).isEqualTo(game.getSecretWord());
        assertThat(result.getAttempts()).isEqualTo(game.getAttempts());

    }
}
