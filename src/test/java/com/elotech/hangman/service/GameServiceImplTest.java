package com.elotech.hangman.service;

import com.elotech.hangman.entity.Game;
import com.elotech.hangman.repository.ConfigurationJpaRepository;
import com.elotech.hangman.service.Impl.GameServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.BDDMockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class GameServiceImplTest {

    @Mock
    private ConfigurationJpaRepository configurationJpaRepository;

    @InjectMocks
    private GameServiceImpl gameServiceImpl;

    @Test
    public void shouldExecuteWithTheFirstLetter() {
        String strings[] = {"_", "_", "_", "_", "_"};
        final var game = Game
                .builder()
                .attempts(3)
                .id(1L)
                .discoveryLetter(strings)
                .secretWord("amigo")
                .build();

        given(configurationJpaRepository.findById(game.getId())).willReturn(Optional.of(game));

        given(configurationJpaRepository.save(any())).willReturn(game);

        final var result = gameServiceImpl.execute(1L, "a");

        final var argumentCaptor = ArgumentCaptor.forClass(Game.class);

        verify(configurationJpaRepository).findById(game.getId());
        verify(configurationJpaRepository).save(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue().getAttempts()).isEqualTo(2);

        assertThat(result).isEqualTo("a____ letter entered successfully");
    }

    @Test
    public void shouldNotExecuteWithTheFirstLetterAndThrowException() {
        final var game = Game
                .builder()
                .attempts(3)
                .id(1L)
                .letter("A")
                .secretWord("Amigo")
                .build();

        given(configurationJpaRepository.findById(game.getId())).willReturn(Optional.of(game));

        given(configurationJpaRepository.save(game)).willReturn(game);

        try {
            final var result = gameServiceImpl.execute(1L, "A");
            Assert.fail("Exception expected!");
        }catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage()).isEqualTo("this letter has already been informed!! You still have " + 2 + " attempts.");
        }

        verify(configurationJpaRepository).findById(game.getId());
        verify(configurationJpaRepository).save(any());
    }

    @Test
    public void shouldThrowExceptionBecauseLetterNotExist() {
        final var game = Game
                .builder()
                .attempts(3)
                .id(1L)
                .letter("")
                .secretWord("Amigo")
                .build();

        given(configurationJpaRepository.findById(game.getId())).willReturn(Optional.of(game));
        given(configurationJpaRepository.save(any())).willReturn(game);

        try {
            final var result = gameServiceImpl.execute(1L, "F");
            Assert.fail("Exception expected!");
        }catch (IllegalArgumentException ex) {
            assertThat(ex.getMessage()).isEqualTo("The letter entered not exists. You still have " + 2 + " attempts.");
        }

        verify(configurationJpaRepository).findById(game.getId());
        verify(configurationJpaRepository).save(any());
    }

    @Test
    public void shouldGetAttempts() {
        final var game = Game
                .builder()
                .attempts(3)
                .id(1L)
                .letter("")
                .secretWord("Amigo")
                .build();

        given(configurationJpaRepository.findAllById(1L)).willReturn(game);

        final var result = gameServiceImpl.getAttempts(1L);

        assertThat(result).isEqualTo("You still have " + 3 + " attempts");

        verify(configurationJpaRepository).findAllById(1L);
    }

    @Test
    public void shouldThrowExceptionBecauseNotFoundById() {
        final var game = Game
                .builder()
                .attempts(3)
                .id(1L)
                .letter("")
                .discoveryLetter(new String[5])
                .secretWord("Amigo")
                .build();

        try {
            final var result = gameServiceImpl.execute(3L, "A");
            Assert.fail("Exception expected!");
        }catch (EntityNotFoundException ex) {
            assertThat(ex.getMessage()).isEqualTo("Game not found");
        }

        verify(configurationJpaRepository).findById(any(Long.class));
    }
}
