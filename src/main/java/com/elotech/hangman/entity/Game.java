package com.elotech.hangman.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "game_configuration")
@Builder
@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Game implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "secret_word", nullable = false)
    private String secretWord;

    @Column(name = "attempts", nullable = false)
    private int attempts;

    @Column(name = "letter")
    @Builder.Default
    private String letter = "";

    @Column(name = "discovery_letter")
    private String[] discoveryLetter;

}

