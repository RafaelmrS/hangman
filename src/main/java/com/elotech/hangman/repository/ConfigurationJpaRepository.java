package com.elotech.hangman.repository;

import com.elotech.hangman.entity.Game;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConfigurationJpaRepository extends JpaRepository<Game, Long> {
    Game findAllById(Long gameId);
}
