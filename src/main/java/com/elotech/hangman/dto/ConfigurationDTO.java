package com.elotech.hangman.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import lombok.*;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ConfigurationDTO {

    @NotNull
    @JsonProperty(value = "secretWord")
    private String secretWord;

    @NotNull
    @JsonProperty(value = "attempts")
    private int attempts;

}
