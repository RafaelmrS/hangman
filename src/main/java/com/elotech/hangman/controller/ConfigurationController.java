package com.elotech.hangman.controller;

import com.elotech.hangman.dto.ConfigurationDTO;
import com.elotech.hangman.service.ConfigurationService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/v1/configuration", produces = "application/json")
@AllArgsConstructor
public class ConfigurationController {
    private ConfigurationService configurationService;

    @PostMapping
    public ConfigurationDTO save(@RequestBody ConfigurationDTO configurationDTO) {
        return configurationService.save(configurationDTO);
    }
}
