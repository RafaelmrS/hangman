package com.elotech.hangman.controller;

import com.elotech.hangman.dto.LetterDTO;
import com.elotech.hangman.service.GameService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/v1/game", produces = "application/json")
@AllArgsConstructor
public class GameController {
    private GameService gameService;

    @PostMapping(path = "/{gameId}")
    public String execute(@PathVariable Long gameId, @RequestBody LetterDTO dto) {
        return gameService.execute(gameId, dto.getInputLetter().toLowerCase());
    }

    @GetMapping(path = "/{gameId}")
    public String getAttempts(@PathVariable Long gameId) {
        return gameService.getAttempts(gameId);
    }

}
