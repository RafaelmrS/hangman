package com.elotech.hangman.service;

import com.elotech.hangman.dto.ConfigurationDTO;

public interface ConfigurationService {

    ConfigurationDTO save(ConfigurationDTO configurationDTO);
}
