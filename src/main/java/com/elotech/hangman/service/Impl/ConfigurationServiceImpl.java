package com.elotech.hangman.service.Impl;

import com.elotech.hangman.dto.ConfigurationDTO;
import com.elotech.hangman.entity.Game;
import com.elotech.hangman.repository.ConfigurationJpaRepository;
import com.elotech.hangman.service.ConfigurationService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ConfigurationServiceImpl implements ConfigurationService {
    private ConfigurationJpaRepository configurationJpaRepository;

    @Override
    public ConfigurationDTO save(ConfigurationDTO configurationDTO) {
        final var gameConfiguration = Game
                .builder()
                .attempts(configurationDTO.getAttempts())
                .secretWord(configurationDTO.getSecretWord().toLowerCase())
                .discoveryLetter(format(configurationDTO.getSecretWord().toLowerCase()))
                .build();
        final var game = configurationJpaRepository.save(gameConfiguration);
        return configurationDTO;
    }

    private String[] format(String secretWord) {
        String[] trace = new String[secretWord.length()];
        for (int i = 0; i < secretWord.length(); i++) {
            trace[i] = "_";
        }
        return trace;
    }
}
