package com.elotech.hangman.service.Impl;

import com.elotech.hangman.entity.Game;
import com.elotech.hangman.repository.ConfigurationJpaRepository;
import com.elotech.hangman.service.GameService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class GameServiceImpl implements GameService {
    private ConfigurationJpaRepository jpaRepository;

    @Override
    public String execute(Long gameId, String inputLetter) {
        final Game game = findById(gameId);

        final var secretWord = game.getSecretWord().toLowerCase();

        checkAlreadyInformed(inputLetter, game);

        var discoveryLetter = game.getDiscoveryLetter();

        int sizeWord = secretWord.length();

        if (checkContains(inputLetter, secretWord)) {
            for (int i = 0; i < discoveryLetter.length; i++) {
                if (secretWord.charAt(i) == inputLetter.charAt(0)) {
                    discoveryLetter[i] = inputLetter;
                    sizeWord--;
                }
            }
        } else {
            final var attempts = updateAttempts(game, inputLetter, discoveryLetter);
            throw new IllegalArgumentException("The letter entered not exists. You still have " + attempts + " attempts.");
        }
        if (sizeWord <= 0 || formatOutputString(discoveryLetter).toString().equals(secretWord)) {
            return "Congratulations you won! The word is: " + game.getSecretWord();
        }
        updateAttempts(game, inputLetter, discoveryLetter);
        return formatOutputString(discoveryLetter) + " letter entered successfully";
    }

    @Override
    public String getAttempts(Long gameId) {
        final var attempts = jpaRepository.findAllById(gameId).getAttempts();
        return "You still have " + attempts + " attempts";
    }

    private Game findById(Long gameId) {
        return jpaRepository.findById(gameId)
                .stream()
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Game not found"));
    }

    private StringBuilder formatOutputString(String[] discoveryLetter) {
        StringBuilder outputString = new StringBuilder();
        for (String s : discoveryLetter) {
            outputString.append(s);
        }
        return outputString;
    }

    private void checkAlreadyInformed(String inputLetter, Game game) {
        final var letter = game.getLetter();
        for (int i = 0; i < letter.length(); i++) {
            if (letter.contains(inputLetter)) {
                final var attempts = updateAttempts(game, inputLetter, game.getDiscoveryLetter());
                throw new IllegalArgumentException("this letter has already been informed!! You still have " + attempts + " attempts.");
            }
        }
    }

    private boolean checkContains(String inputLetter, String secretWord) {
        if (secretWord.contains(inputLetter)) {
            return true;
        }
        return false;
    }

    private int updateAttempts(Game game, String letter, String[] discoveryLetter) {
        game.setLetter(game.getLetter() + letter);
        game.setAttempts(game.getAttempts() - 1);
        game.setDiscoveryLetter(discoveryLetter);
        jpaRepository.save(game);
        return game.getAttempts();
    }
}
