package com.elotech.hangman.service;

public interface GameService {

    String execute(Long gameId, String inputLetter);

    String getAttempts(Long gameId);
}
